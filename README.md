## Installation

- Before doing installation steps, update (if required)
  - mysqlchk.socket: Socket/ListenStream to approproate value (e.g. strictly specify IP:port)
  - mysqlchk: MySQL access parameters

- Installation steps (on every node where MySQL is running)
  - install -m 644 -o root mysqlchk.socket /etc/systemd/system
  - install -m 644 -o root mysqlchk@.service /etc/systemd/system
  - mkdir -p /opt
  - install -m 755 -o root mysqlchk /opt
  - systemctl daemon-reload
  - systemctl enable mysqlchk.socket
  - systemctl start mysqlchk.socket

- Update haproxy configuration (/etc/haproxy/haproxy.cfg) with content of haproxy-mysql.cfg
  - ports / hostnames need to be reviewed and changed if required
  - systemctl reload haproxy

- You are all done
